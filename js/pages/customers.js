var $ = require('jquery');

function Customers() {
    this.pageEl = document.querySelector('#customers-page');
    this.$formEl = this.$pageEl.find('form');
    this.$areaEl = this.$pageEl.find('textarea');
    this.$listEl = this.$pageEl.find('.customers__list');

    this.init = function() {
        this.$formEl.submit($.proxy(this, 'submit'));
    };

    this.submit = function() {
        var areaValue = this.$areaEl.val();
        if (!areaValue) {
            alert('No way, dude! Write some stuff!');

            return false;
        }

        $.post('/customers', { feedbackText: areaValue }, function(data) {

        });

        this.$listEl.append('<div class="customers__feedback">' + areaValue +' </div>');

        this.$areaEl.val('');

        return false;
    };

    this.init();
}

module.exports = Customers;