TO RUN SERVER LOCALLY YOU NEED:

1. install NODE.JS version >= 4.4.7 on your machine

2. go to root directory (where the 'package.json' file located)

3. run command "npm install" to install all dependencies

4. run command "npm run build-js" to build js 

5. run command "npm run watch-js" to watch js changes

6. run command "npm run watch-less" to watch less changes

7. run command "npm run start" to run server on localhost:8080 (see "bin/config to change port number")

HAVE FUN! :)