var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

router.get('/', function(req, res) {
    res.render('./pages/customers', {
		layout: 'layout',
		title: 'a3boot | For Customers',
		page: 'CUSTOMERS'
	});
});

router.post('/', function(req, res) {
    console.log(req.body.feedbackText);
    res.end('ok');
});


module.exports = router;
