var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('./pages/about', {
		layout: 'layout',
		title: 'a3boot | About Us',
		page: 'ABOUT'
	});
});

module.exports = router;
