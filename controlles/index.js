var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
	res.render('./pages/index', {
		layout: 'layout',
		title: 'a3boot | News',
		page: 'NEWS'
	});
});

module.exports = router;
